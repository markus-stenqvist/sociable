//var socket = window.location.href.indexOf("localhost") > -1 ? io.connect('http://localhost') : io.connect('http://sociabel.nu');
var socket = window.location.href.indexOf("localhost") > -1 ? io.connect('http://localhost') : io.connect('//' + window.location.host + ':8000');

$(function() {

	var room = '';
	var myPhoto = false;

	var findMatch = function () {
		if (uid) {
			NProgress.start();
			$('#talking-to').text('Letar partner..');

			socket.emit('join-lobby', {uid: uid}, function(data) {
				if(data) {
					alert(data.error);
				} else {
					$('.join-lobby').hide();
				}
			});
		} else {
			alert("Du måste logga in först!");
		}
	};

	var newMatchShow = function () {
		$('#new-match').fadeIn('slow');
	};

	socket.on('set-detail', function(data) {
		var photo = $('.profile-photo');
		myPhoto = myPhoto ? myPhoto : photo.prop('src');
		photo.prop('src', data.photo);
		$('#talking-to').text('Du chattar nu med ' + data.name);
	});

	socket.on('match-found', function (data) {
		room = data.room;
		$('#match-loading-text').remove();
		var chatArea = '<div id="chat" class"col-12"></div>';
		var chatInput = '<div class="col-3" id="chat-input-container"><input id="chat-input" class="field" type="text" placeholder="Skriv här.. (enter för att skicka)">';
		$('#conversation').append(chatArea).append(chatInput);
		$('#chat-once').remove();
		$('#new-match').text('Hitta en ny partner!').hide();
		NProgress.done();
		setTimeout(newMatchShow, 4000);
	});

	socket.on('s-message', function (data) {
		var chat = $('#chat');
		chat.append('<p class="chat-message"><strong>' + data.first_name + '</strong>: ' + data.message + '</p>');
		chat.scrollTo('130%', 300 );
		$.titleAlert(data.first_name + " skrev något..", {
		    requireBlur:true,
		    stopOnFocus:true,
		    duration:20000,
		    interval:750
		});
	});

	socket.on('s-end', function (data) {
		var chat = $('#chat');
		chat.append('<p class="chat-message">' + data.first_name + ' ' + data.message + '</p>');
		chat.scrollTo('130%', 300 );
		$.titleAlert(data.first_name + " skrev något..", {
		    requireBlur:true,
		    stopOnFocus:true,
		    duration:20000,
		    interval:750
		});
	});

	socket.on('new-match', function (data) {
		findMatch();
	});


	$('.join-lobby').click(function (event) {
		event.preventDefault();
		findMatch();
	});

	$('body').on('click', '#new-match', function (event) {
		event.preventDefault();
		$('#chat').remove();
		$('#chat-input-container').remove();
		$('#new-match').hide();
		socket.emit('new-match', {room: room});
		$('.profile-photo').prop('src', myPhoto).fadeIn();
		NProgress.start();
	});

	$('body').on('keyup', '#chat-input', function (event) {
		var code = event.which;
		if (code == 13) {
			var message = $(this).val();
			if (message.trim().length > 0) {
				$(this).val('');
				socket.emit('u-message', {room: room, message: message});
			}
		}
	});
});
