var mongoose = require('mongoose'),
    findOrCreate = require('mongoose-findorcreate');
require('mongoose-long')(mongoose);
var SchemaTypes = mongoose.Schema.Types;



var userModel = function () {
    var UserSchema = mongoose.Schema({
    	username: String,
    	uid: SchemaTypes.Long,
    	access_token: String,
    	access_token_secret: String,
        first_name: String,
        last_name: String,
        photo: String,
        email: String,
        presentation: String,
        gender: String,
        role: String,
    	created: Date,
    	updated: Date
    });

    UserSchema.plugin(findOrCreate);


    UserSchema.pre('save', function (next) {
        // TODO ADD SOMETHING HERE
        //var user = this;

        //Continue with the save operation
        next();
    });

    return mongoose.model('User', UserSchema);
};

module.exports = new userModel();
