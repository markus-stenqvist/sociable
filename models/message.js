var mongoose = require('mongoose');

var messageModel = function () {
    var MessageSchema = mongoose.Schema({
        conversation_id: String,
        author: String,
        message: String,
        created: Date
    });


    MessageSchema.pre('save', function (next) {
        // TODO ADD SOMETHING HERE
        //var user = this;

        //Continue with the save operation
        next();
    });

    return mongoose.model('Message', MessageSchema);
};

module.exports = new messageModel();
