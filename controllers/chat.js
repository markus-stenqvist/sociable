'use strict';


var ChatModel = require('../models/chat');


module.exports = function (app) {

    var model = new ChatModel();


    app.get('/chat', function (req, res) {
        
        res.format({
            json: function () {
                res.json(model);
            },
            html: function () {
                res.render('chat', model);
            }
        });
    });

};
