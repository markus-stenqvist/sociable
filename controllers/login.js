'use strict';


var LoginModel = require('../models/login'),
    passport = require('passport');


module.exports = function (app) {

    var model = new LoginModel();


    app.get('/login', function (req, res) {
         //Include any error messages that come from the login process.
        model.messages = req.flash('error');
        res.render('login', model);
    });

    app.get('/auth/facebook', passport.authenticate('facebook'));

    app.get('/auth/facebook/callback', passport.authenticate('facebook', { failureRedirect: '/' }), function(req, res) {
        // Successful authentication, redirect home.
        res.redirect('/');
    });

    app.get('/logout', function (req, res) {
        req.logout();
        res.redirect('/');
    });

};
