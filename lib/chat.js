

exports.initLobby = function() {
	return Lobby = {
		users: []

		, join: function (socket) {
			this.users.push(socket);
			console.log("User with uid: " + socket.uid + " JOINED the lobby");
		}

		, leave: function (socket) {
			var index = this.users.indexOf(socket);
			if (index !== -1)
				this.users.splice(index, 1);
			console.log("User with uid: " + socket.uid + " LEFT the lobby");
		}

		, match: function() {
			if (this.users.length != 0) {
				for (socket in this.users) {
					console.log("MATCH!");
					if (this.users[socket]) {
						return this.users[0];
					}
				}
				return false;
			} else {
				return false;
			}
		}
	};
}
