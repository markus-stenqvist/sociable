var chat = require('./chat');
var socketio = require('socket.io'),
    auth = require('./auth'),
    passportSocketIo = require("passport.socketio"),
    Message = require('../models/message');

var tagBody = '(?:[^"\'>]|"[^"]*"|\'[^\']*\')*';

var tagOrComment = new RegExp(
    '<(?:'
    // Comment body.
    + '!--(?:(?:-*[^->])*--+|-?)'
    // Special "raw text" elements whose content should be elided.
    + '|script\\b' + tagBody + '>[\\s\\S]*?</script\\s*'
    + '|style\\b' + tagBody + '>[\\s\\S]*?</style\\s*'
    // Regular name
    + '|/?[a-z]'
    + tagBody
    + ')>',
    'gi');

module.exports.listen = function(app, express, sessionStore, passport) {
    var Lobby = chat.initLobby();
    io = socketio.listen(app);

    // set authorization for socket.io
    io.set('authorization', passportSocketIo.authorize({
        passport: passport,
        cookieParser: express.cookieParser,
        key:         'sess',       // the name of the cookie where express/connect stores its session_id
        secret:      'superhemlig12',    // the session_secret to parse the cookie
        store:       sessionStore,        // we NEED to use a sessionstore. no memorystore please
        success: function (data, accept) {

            // The accept-callback still allows us to decide whether to
            // accept the connection or not.
            accept(null, true);
        },
        fail: function (data, message, error, accept) {
            if(error)
                throw new Error(message);
            console.log('failed connection to socket.io:', message);

            // We use this callback to log all of our failed connections.
            accept(null, false);
        }
    }));

    var removeTags = function (html) {
        var oldHtml;
        do {
            oldHtml = html;
            html = html.replace(tagOrComment, '');
        } while (html !== oldHtml);
        return html.replace(/</g, '&lt;');
    };

    var getUniqueID = function () {
      // Math.random should be unique because of its seeding algorithm.
      // Convert it to base 36 (numbers + letters), and grab the first 9 characters
      // after the decimal.
      return '_' + Math.random().toString(36).substr(2, 9);
    };

    io.sockets.on('connection', function (socket) {

        socket.on('join-lobby', function (data, callback) {

            if (data.uid) {
                var match = Lobby.match();
                if (match) {
                    Lobby.leave(match);
                    var room = getUniqueID();
                    match.join(room);
                    socket.join(room);
                    socket.emit('set-detail', {name: match.handshake.user.first_name, photo: match.handshake.user.photo});
                    match.emit('set-detail', {name: socket.handshake.user.first_name, photo: socket.handshake.user.photo});
                    io.sockets.in(room).emit('match-found', {room: room});
                } else {
                    socket.uid = data.uid;
                    Lobby.join(socket);
                }

                callback();
            } else {
                callback({error: "Du måste logga in först!", message: false});
            }
        });

        socket.on('u-message', function (data) {
            io.sockets.in(data.room).emit('s-message', {first_name: socket.handshake.user.first_name, message: removeTags(data.message)});
            Message.create({
                conversation_id: data.room,
                author: socket.handshake.user.username,
                message: removeTags(data.message),
                created: new Date()
            }, function(err, message) {
                if (err) console.log(err);
            });
        });

        socket.on('new-match', function (data) {
            socket.leave(data.room);
            io.sockets.in(data.room).emit('s-end', {first_name: socket.handshake.user.first_name, message: 'har avslutat chatten'});
            socket.emit('new-match', {});
        });

        socket.on('disconnect', function () {
            if (socket.uid !== undefined) {
                Lobby.leave(socket);
                var rooms = io.sockets.manager.roomClients[socket.id];
                console.log("User disconnected!");
                for (key in rooms) {
                    if(key !== "")
                        io.sockets.in(key).emit('s-end', {first_name: socket.handshake.user.first_name, message: "har avslutat chatten"});
                }
            }
        });
    });
    
    return io;
};
