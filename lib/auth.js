var FacebookStrategy = require('passport-facebook').Strategy;
var findOrCreate = require('mongoose-findorcreate');
var User = require('../models/user');
var debug = true;

exports.facebookStrategy = function () {

	return new FacebookStrategy({
		clientID: debug ? '671401062901731' : '202827066557334',
		clientSecret: debug ? 'f66d06a138b3ac887ac6f0ced365e92c' : 'd8d27a000156c3d4f8750689a526fbbb',
		callbackURL: debug ? 'http://localhost:8000/auth/facebook/callback' : "http://sociabel.nu/auth/facebook/callback"
  	},
	function(accessToken, refreshToken, profile, done) {
		User.findOrCreate({uid: profile.id},
			{
				username: profile.username,
				gender: profile.gender,
				first_name: profile.name.givenName,
				last_name: profile.name.familyName,
				photo: 'https://graph.facebook.com/' + profile.id + '/picture?type=large',
				access_token: accessToken
			},
			function(err, user) {
				if (err) { return done(err); }
				done(null, user);
		});
	});

}

exports.isAuthenticated = function (role) {

    return function (req, res, next) {

        if (!req.isAuthenticated()) {

            //If the user is not authorized, save the location that was being accessed so we can redirect afterwards.
            req.session.goingTo = req.url;
            res.redirect('/chat');
            return;
        }

        //If a role was specified, make sure that the user has it.
        if (role && req.user.role !== role) {
            res.status(401);
            res.render('errors/401');
        }

        next();
    }
}

exports.injectUser = function (req, res, next) {
    if (req.isAuthenticated()) {
        res.locals.user = req.user;
    }
    next();
}
