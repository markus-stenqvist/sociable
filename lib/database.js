'use strict';
var mongoose = require('mongoose');

var db = function () {
	var db = false;
	return {

		config: function (conf) {
			mongoose.connect('mongodb://' + conf.host + '/' + conf.database);
			db = mongoose.connection;
			db.on('error', console.error.bind(console, 'connection error:'));
			db.once('open', function callback() {
				console.log('db connection open');
			});
		},

		get: function() {
			return db;
		}
	};
};

module.exports = db();