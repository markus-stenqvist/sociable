'use strict';


var kraken = require('kraken-js'),
    db = require('./lib/database'),
    passport = require('passport'),
    User = require('./models/user'),
    auth = require('./lib/auth'),
    flash = require('connect-flash'),
    express = require('express'),
    connect = require('connect'),
    MongoStore = require('connect-mongo-store')(connect),
    mongostore = false,
    app = {};


app.configure = function configure(nconf, next) {
    // Async method run on startup.
    //Configure the database
    db.config(nconf.get('databaseConfig'));
    mongostore = new MongoStore('mongodb://localhost:27017/sociable');

    //Tell passport to use our newly created local strategy for authentication
    passport.use(auth.facebookStrategy());

    //Give passport a way to serialize and deserialize a user. In this case, by the user's id.
    passport.serializeUser(function (user, done) {
        console.log(user);
        done(null, user.uid);
    });

    passport.deserializeUser(function (id, done) {
        User.findOne({uid: id}, function (err, user) {
            if (err) { console.log("Could not deserialize user!"); }
            done(null, user);
        });
    });

    next(null);
};


app.requestStart = function requestStart(server) {
    // Run before most express middleware has been registered.
    server.use(express.cookieParser());
    server.use(express.session({store: mongostore, secret: 'superhemlig12', key: 'sess'})); 
    server.use(passport.initialize());  //Use Passport for authentication
    server.use(passport.session());     //Persist the user in the session
    server.use(flash());                //Use flash for saving/retrieving error messages for the user
    server.use(auth.injectUser);        //Inject the authenticated user into the response context
};


app.requestBeforeRoute = function requestBeforeRoute(server) {
    // Run before any routes have been added.
};


app.requestAfterRoute = function requestAfterRoute(server) {
    // Run after all routes have been added.
};


if (require.main === module) {
    kraken.create(app).listen(function (err, server) {
        if (err) {
            console.error(err);
        }
        var io = require('./lib/socket').listen(server, express, mongostore, passport);
    });
}


module.exports = app;